
(in-package :cl-powercode)

(defvar *powercode-server-uri* ""
  "Hostname/IP address of the Powercode instance on port 444")
(defvar *powercode-api-key* nil
  "The API key used to authorize the connection. Default is nil, and using it
in its current state will result in an explicit failure.")

(defun gen-request (api-key action requests)
  (remove-if #'null
   (append
    (list "apiKey" api-key)
    (list "action" action)
    requests)))

(defun make-request (api-key action requests)
  (unless api-key (error "No apiKey defined!"))
  (encode-json-plist-to-string
   (gen-request api-key action requests)))

(defun format-requests (request-list)
  (iterate (for elt in request-list)
    (if (symbolp elt)
        (collect (lisp-to-camel-case (symbol-name elt)))
        (collect elt))))

(defun send-request (host request-body)
  (let ((cookie-jar (make-instance 'cookie-jar))) 
    (http-request host
                  :accept "application/json"
                  :method :post
                  :content-type "application/json"
                  :external-format-out :utf-8
                  :external-format-in  :utf-8
                  :redirect 100
                  :cookie-jar cookie-jar
                  :content request-body)))

(defun powercode-do (action requests &key (host *powercode-server-uri*)
                                          (key *powercode-api-key*))
  (let ((act-string (lisp-to-camel-case (symbol-name action))))
    (decode-json-from-string
     (octets-to-string
      (send-request host (make-request key act-string (format-requests requests)))
      :external-format :utf-8))))

(defun check-server ()
  (powercode-do :check '()))

(defun read-customer (number)
  (powercode-do :read-customer
                (list "customerID" number)))

(defun search-customers (term)
  (powercode-do :search-customers
                (list :search-string term)))

(defun update-customer (id new-data) 
  (powercode-do (update-or-status new-data)
                (append (list "customerID" id)
                        new-data)))

(defun update-or-status (data-list)
  (if (member :status data-list :test 'eql)
      :update-customer-status
      :update-customer))
