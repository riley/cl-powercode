
(asdf:defsystem #:powercode
  :serial t
  :description "Use Powercode from Common Lisp"
  :version "1.0"
  :author "Riley Eltrich <rileye@rockisland.com>"
  :licence "LGPL v3.0+"
  :depends-on (:iterate :flexi-streams :cl-json :drakma)
  :components ((:file "package")
               (:file "powercode")
               (:file "powercode-supplemental")
               (:file "conf")))
