
(defpackage #:cl-powercode
  (:nicknames #:pc #:cl-pc #:powercode)
  (:use :iterate :cl)
  (:import-from :flexi-streams :octets-to-string)
  (:import-from :drakma :http-request
                        :cookie-jar)
  (:import-from :cl-json :decode-json-from-string
                         :encode-json-plist-to-string
                         :lisp-to-camel-case)
  (:export :powercode-do
           :read-customer
           :search-customers
           :update-customer
           :find-duplicates
           :enum-duplicates
           :del-duplicates
           :customer-p
           :*powercode-server-uri*
           :*powercode-api-key*))
