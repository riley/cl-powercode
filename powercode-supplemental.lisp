
(in-package :cl-powercode)

(defun batch-update (range new-data)
  (iterate (for id from (car range) to (cadr range))
    (when (customer-p id)
      (update-customer id new-data))))

(defun customer-p (id)
  (let ((customers (cdr (assoc :customers (search-customers id))))) 
    (iterate (for customer in customers)
      (if (and (ignore-errors (read-customer id))
               (valid-p id customer))
          (return t)
          (return nil)))))

(defun valid-p (id customer-entry)
  (and (not (string= (cdr (assoc :*address-1 customer-entry)) ""))
       (string= (cdr (assoc :*customer-+id+ customer-entry))
                (princ-to-string id))))

(defun real-customer-p (customer)
  (let ((status-bag (list "active" "lead" "prospect" "collections"
                          "committed lm" "committed mm" "pre install"
                          "not active" "delinquent" "uninstall")))
    (if (member (cdr (assoc :status (cdr (assoc :customer customer))))
                status-bag :test 'equalp)
        customer
        nil)))

(defun collect-valid-customers (range)
  (remove-duplicates
   (iterate (for i from (car range) to (cadr range))
     (when (and (customer-p i)
                (real-customer-p (read-customer i)))
       (collecting (assoc :customer (read-customer i)))))
   :test 'equalp))

(defun in-db? (customer db)
  (flet ((pstreet (u)
           (cdr (assoc :physical-street (cdr u)))))
    (iterate (for db-entry in db)
      (when (equalp (pstreet customer)
                    (pstreet db-entry))
        (collect db-entry)))))

(defun find-duplicates (user-list)
  (iterate (for customer in user-list)
    (let ((dup-list (in-db? customer user-list)))
      (when (<= 2 (length dup-list))
        (collect (list (cdr (assoc :customer-id (cdr customer)))
                       dup-list))))))

(defun enum-duplicates (dup-catalog)
  (iterate (for elt in dup-catalog)
    (collecting
     (list (car elt)
           (iterate (for dup in (cdadr elt))
             (collect (list (cdr (assoc :customer-id (cdr dup)))
                            (cdr (assoc :company-name (cdr dup)))
                            (cdr (assoc :physical-street (cdr dup))))))))))

(defun del-duplicates (dup-catalog)
  (iterate (for elt in dup-catalog)
    (iterate (for dup in (cdadr elt))
      (update-customer (cdr (assoc :customer-id (cdr dup)))
                       '(:status "Delete")))))
